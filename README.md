# Preserving Entity References
*The instructions here assume that the sample resources have been checked-out, cloned or downloaded and unzipped into the samples directory of the XML-Compare release. The resources should be located such that they are two levels below the top level release directory that contains the jar files.*

*For example `DeltaXML-XML-Compare-10_0_0_j/samples/sample-name`.*

---

How to retain entity references in the comparison output.

This document describes how to run the sample. For concept details see: [Preserving Entity References](https://docs.deltaxml.com/xml-compare/latest/samples-and-guides/preserving-entity-references)

## Pipelined Comparator Java API
If you have Ant installed, use the build script provided to run the sample (found in directory samples/PreserveEntityRefs). Simply type the following command to run the pipeline and produce the output files result.xml.

	ant run
	
To clean up the sample directory, run the following command in Ant.

	ant clean

## Document Copmparator DCP Configuration
If you don't have Ant installed, you can run the sample from a command line by issuing the following command from the sample directory (ensuring that you use the correct directory separators for your operating system).  Replace x.y.z with the major.minor.patch version number of your release e.g. command-10.0.0.jar

	java -jar ../../command-x.y.z.jar compare preserve-er input1.html input2.html result-def-dcp.html mode=def
	java -jar ../../command-x.y.z.jar compare preserve-er input1.html input2.html result-ecs-dcp.html mode=ecs
	java -jar ../../command-x.y.z.jar compare preserve-er input1.html input2.html result-nes-dcp.html mode=nes
	java -jar ../../command-x.y.z.jar compare preserve-er input1.html input2.html result-rts-dcp.html mode=rts
	