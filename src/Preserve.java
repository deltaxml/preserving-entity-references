// Copyright (c) 2024 Deltaman group limited. All rights reserved.

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import com.deltaxml.core.ComparatorInstantiationException;
import com.deltaxml.core.FeatureNotRecognizedException;
import com.deltaxml.core.FeatureSettingNotSupportedException;
import com.deltaxml.core.FilterConfigurationException;
import com.deltaxml.core.ParserInstantiationException;
import com.deltaxml.core.PipelinedComparatorError;
import com.deltaxml.cores9api.ComparisonCancelledException;
import com.deltaxml.cores9api.ComparisonException;
import com.deltaxml.cores9api.DuplicateStepNameException;
import com.deltaxml.cores9api.FilterChain;
import com.deltaxml.cores9api.FilterProcessingException;
import com.deltaxml.cores9api.FilterStep;
import com.deltaxml.cores9api.FilterStepHelper;
import com.deltaxml.cores9api.LicenseException;
import com.deltaxml.cores9api.PipelineLoadingException;
import com.deltaxml.cores9api.PipelineSerializationException;
import com.deltaxml.cores9api.PipelinedComparatorS9;
import com.deltaxml.cores9api.config.AdvancedEntityRefUsage;
import com.deltaxml.cores9api.config.LexicalPreservationConfig;
import com.deltaxml.cores9api.config.PreservationProcessingMode;

/**
 * A simple class for preserving processing instructions and comments, when using a 'Core S9API' pipelined comparator.
 */
public class Preserve {

  /**
   * Compare two input files and return a result file.
   */
  public static void main(String[] args) {
    if (args.length != 4) {
      System.out.println("java -cp <deltaxml.jar>:class Preserve <in1> <in2> <out> <mode>");
      System.exit(1);
    }

    try {
      // Use the base pre-defined preservation mode, which essentially provides a
      // neutral starting point, where only text, attribute and element nodes are
      // being preserved.
      LexicalPreservationConfig lpc= new LexicalPreservationConfig("base");

      // Specify the type of entity reference support to be provided.
      // The default case is for all theses versions to be false.
      boolean nestedEntitySupport= "nes".equals(args[3]);
      boolean entityChangeSupport= nestedEntitySupport || "ecs".equals(args[3]);
      boolean replacementTextSupport= "rts".equals(args[3]);

      // Enable comments and processing instructions to be converted into
      // elements for the purposes of comparison.
      lpc.setPreserveEntityReplacementText(entityChangeSupport || replacementTextSupport);
      lpc.setPreserveEntityReferences(true);
      lpc.setPreserveNestedEntityReferences(nestedEntitySupport);
      lpc.setPreserveDoctype(!replacementTextSupport);

      // Keep the entity references, otherwise leave all other preserved items alone.
      lpc.setDefaultProcessingMode(PreservationProcessingMode.CHANGE);
      lpc.setAdvancedEntityReferenceUsage(replacementTextSupport ? AdvancedEntityRefUsage.REPLACE : AdvancedEntityRefUsage.SPLIT);
      lpc.setDoctypeProcessingMode(PreservationProcessingMode.B);

      // This is an unnecessary command as we have effectively set this via the default output mode,
      // however it is worth drawing attention to the fact that we are going to specifically handle
      // changes to entity references with another filter in the chain.
      lpc.setEntityRefProcessingMode(PreservationProcessingMode.CHANGE);

      // Construct a pipelined comparator, then configure its lexical preservation
      // as defined above.
      PipelinedComparatorS9 pc= new PipelinedComparatorS9();
      FilterStepHelper filterStepHelper= pc.newFilterStepHelper();
      pc.setLexicalPreservationConfig(lpc);

      // Construct the filter step for handling changes in entity references (using html markup).
      // This filter has several assumptions about the data format and locations of change,
      // and is designed to work only for the sample data. It is not intended as a general
      // purpose filter.
      FilterStep outputStep= filterStepHelper.newFilterStep(new File("src/example-html-output.xsl"),
                                                            "example-html-output-filter");

      // Add the lexical preservation processing filters before the html output filter.
      FilterChain chain= pc.newLexicalPreservationOutputFilterChain(lpc);
      chain.addStep(outputStep);
      pc.setOutputFilters(chain);

      // w3.org gives 503 errors for DTD requests, we dont actually need it for this sample
      pc.setParserFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);

      // Run the comparison
      pc.compare(new File(args[0]), new File(args[1]), new File(args[2]));

    } catch (ParserInstantiationException e) {
      e.printStackTrace();
    } catch (ComparatorInstantiationException e) {
      e.printStackTrace();
    } catch (ComparisonException e) {
      e.printStackTrace();
    } catch (FilterProcessingException e) {
      e.printStackTrace();
    } catch (PipelineLoadingException e) {
      e.printStackTrace();
    } catch (PipelineSerializationException e) {
      e.printStackTrace();
    } catch (LicenseException e) {
      e.printStackTrace();
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (PipelinedComparatorError e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (FeatureSettingNotSupportedException e) {
      e.printStackTrace();
    } catch (FeatureNotRecognizedException e) {
      e.printStackTrace();
    } catch (IllegalArgumentException e) {
      e.printStackTrace();
    } catch (FilterConfigurationException e) {
      e.printStackTrace();
    } catch (DuplicateStepNameException e) {
      e.printStackTrace();
    } catch (ComparisonCancelledException e) {
      e.printStackTrace();
    }
  }
}
